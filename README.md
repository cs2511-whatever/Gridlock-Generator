# cs2511-whatever/Gridlock-Generator

### On UNIX-like system

To compile, run `./compile`.

To run, run `./run`.

### File locations

UML class diagram is at `doc/GridlockUML.pdf`.

The source of all the images is at `src/view/assets/ref.txt`.

The music used in the game is from a legal copy of RPG maker VX Ace rtp. Track title: Town3

### Design pattern

We've applied MVP (Model-View-Presenter) pattern in this project.
