package whatever.gridlock.view;

import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

import whatever.gridlock.Direction;
import whatever.gridlock.presenter.GamePresenter;


/**
 * Class Vehicle - visual representation of vehicle
 * @author      Zhenyu Yao
 * @author      Xudong Shi
 */
public class Vehicle extends Rectangle {

    private int cordX;
    private int cordY;
    private final int len;
    private final Direction direct;
    private final boolean isPlayer;

    private final ImagePattern orgIm;
    private final ImagePattern dragIm;

    private int orgCordX;
    private int orgCordY;

    private double orgSceneX;
    private double orgSceneY;

    private double positionX;
    private double positionY;

    private final EventHandler<MouseEvent> onMouseEntered = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            ((Vehicle) event.getSource()).setFill(dragIm);
        }
    };

    private final EventHandler<MouseEvent> onMouseExited = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            ((Vehicle) event.getSource()).setFill(orgIm);
        }
    };

    private final EventHandler<MouseEvent> onMousePressed = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            orgCordX = cordX;
            orgCordY = cordY;
            orgSceneX = event.getSceneX();
            orgSceneY = event.getSceneY();

            positionX = orgCordX*50;
            positionY = orgCordY*50;
        }
    };

    private final EventHandler<MouseEvent> onMouseDragged = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            double sceneX = event.getSceneX();
            double sceneY = event.getSceneY();
            if (direct == Direction.HORIZONTAL) {
                ((Vehicle) event.getSource()).setPosX(convert(orgCordX) + sceneX - orgSceneX);
            } else {
                ((Vehicle) event.getSource()).setPosY(convert(orgCordY) + sceneY - orgSceneY);
            }
        }
    };

    private final EventHandler<MouseEvent> onMouseReleased = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            int delta;
            double relativePos;
            if (direct == Direction.HORIZONTAL) {
                relativePos = positionX % 50;
                if (relativePos <= 25) {
                    delta = cordX - orgCordX;
                    ((Vehicle) event.getSource()).setX(convert(cordX));
                }
                else {
                    cordX += 1;
                    delta = cordX - orgCordX;
                    ((Vehicle) event.getSource()).setX(convert(cordX));
                }
            } else {
                relativePos = positionY % 50;
                if (relativePos <= 25) {
                    delta = cordY - orgCordY;
                    ((Vehicle) event.getSource()).setY(convert(cordY));
                }
                else {
                    cordY += 1;
                    delta = cordY - orgCordY;
                    ((Vehicle) event.getSource()).setY(convert(cordY));
                }
            }
            orgCordX = cordX;
            orgCordY = cordY;
            if (Math.abs(delta) > 0) {
                GamePresenter.stepStep();
                GamePresenter.setNewPos((Vehicle) event.getSource(), delta);
            }
            if (isPlayer && cordX == 4) {
                GamePresenter.winGame();
            }
        }
    };

    /**
     * This constructs a vehicle which is not the red car with specified details
     * @param cordX     Vehicle's left most X coordinate in Model
     * @param cordY     Vehicle's up most Y coordinate in Model
     * @param len       Length of Vehicle
     * @param direct    Direction of the vehicle
     * @param imgUrl    Vehicle picture that this one use
     */
    private Vehicle(int cordX, int cordY, int len, Direction direct, String imgUrl) {
        this.len = len;
        this.direct = direct;
        this.isPlayer = false;
        this.setCordX(cordX);
        this.setCordY(cordY);
        if (this.direct == Direction.HORIZONTAL) {
            this.setWidth(50 * this.len);
            this.setHeight(50);
        } else {
            this.setWidth(50);
            this.setHeight(50 * this.len);
        }
        this.setCursor(Cursor.MOVE);
        this.setOnMousePressed(onMousePressed);
        this.setOnMouseDragged(onMouseDragged);
        this.setOnMouseReleased(onMouseReleased);
        this.setOnMouseEntered(onMouseEntered);
        this.setOnMouseExited(onMouseExited);

        this.orgIm = new ImagePattern(new Image(getClass().getResource(imgUrl).toExternalForm()));
        this.setFill(orgIm);
        String draggingUrl = highLightCar(imgUrl);
        this.dragIm =
                new ImagePattern(new Image(getClass().getResource(draggingUrl).toExternalForm()));

        this.orgCordX = cordX;
        this.orgCordY = cordY;
    }

    /**
     * This constructs the red car with specified details
     * @param cordX     Red car's left most coordinate in Model
     * @param imgUrl    Picture red car uses
     */
    private Vehicle(int cordX, String imgUrl) {
        this.len = 2;
        this.direct = Direction.HORIZONTAL;
        this.isPlayer = true;
        this.setCordX(cordX);
        this.setCordY(2);
        this.setWidth(50 * this.len);
        this.setHeight(50);
        this.setCursor(Cursor.MOVE);
        this.setOnMousePressed(onMousePressed);
        this.setOnMouseDragged(onMouseDragged);
        this.setOnMouseReleased(onMouseReleased);
        this.setOnMouseEntered(onMouseEntered);
        this.setOnMouseExited(onMouseExited);

        this.orgIm = new ImagePattern(new Image(getClass().getResource(imgUrl).toExternalForm()));
        this.setFill(orgIm);
        String draggingUrl = highLightCar(imgUrl);
        this.dragIm =
                new ImagePattern(new Image(getClass().getResource(draggingUrl).toExternalForm()));

        this.orgCordX = cordX;
        this.orgCordY = 2;
    }

    /**
     * Find a vehicle's highlighted picture by adding suffix to its picture name
     * @param orgUrl    Original picture of the vehicle
     * @return          The url for highlighted picture of the vehicle
     */
    private String highLightCar(String orgUrl) {
        String[] orgStr = orgUrl.split("");
        int indexOfDot = 0;
        for(int i = 0; i < orgStr.length; i++) {
            if (orgStr[i].equals(".")) {
                indexOfDot = i;
                break;
            }
        }
        StringBuilder orgSb = new StringBuilder(orgUrl);
        orgSb.insert(indexOfDot, "_highlighted");
        return orgSb.toString();
    }

    /**
     * Wrap the constructor of red car above
     * @param cordX     Red car's left most coordinate in Model
     * @param imgUrl    Picture red car uses
     * @return          A red car instance
     */
    public static Vehicle redCar(int cordX, String imgUrl) {
        return new Vehicle(cordX, imgUrl);
    }

    /**
     * Wrap the constructor of a non red car above
     * @param cordX     Vehicle's left most X coordinate in Model
     * @param cordY     Vehicle's up most Y coordinate in Model
     * @param len       Length of Vehicle
     * @param direct    Direction of the vehicle
     * @param imgUrl    Vehicle picture that this one use
     * @return          A non red car instance
     */
    public static Vehicle normalCar(int cordX, int cordY, int len,
                                    Direction direct, String imgUrl) {
        return new Vehicle(cordX, cordY, len, direct, imgUrl);
    }

    /**
     * This convert the coordinate of the car in gameboard in Model
     * to the coordinate in the gameboard in View
     * @param cord      The coordinate in Model to convert
     * @return          The corresponding coordinate in actual game
     */
    private double convert(int cord) {
        return (double) (cord * 50);
    }

    /**
     * This convert the coordinate of the car in View
     * to coordinate in game board in Model
     * @param pos       The coordinate in View
     * @return          Corresponding coordinate in Model
     */
    private int convert(double pos) {
        return ((int) pos) / 50;
    }

    /**
     * This set the X coordinate in model of the car
     * @param cordX     X coordinate
     */
    private void setCordX(int cordX) {
        this.cordX = cordX;
        this.setX(convert(cordX));
        if (this.isPlayer && this.cordX == 4)
            GamePresenter.winGame();
    }

    /**
     * This set the Y coordinate in model of the car
     * @param cordY     Y coordinate
     */
    private void setCordY(int cordY) {
        this.cordY = cordY;
        this.setY(convert(cordY));
    }

    /**
     * This set the horizontal position of the car in the game
     * If the car meets obstacle and bound of gameboard, the car will not move
     * @param posX      Horizontal position that the car is going to move to
     */
    private void setPosX(double posX) {
        if (posX < this.getCordLowerBound() * 50 ||
                posX > (this.getCordUpperBound() + 1 - this.len) * 50) return;
        this.cordX = convert(posX);
        this.positionX = posX;
        this.setX(posX);
    }

    /**
     * This set the vertical position of the car in the game
     * If the car meets obstacle and bound of gameboard, the car will not move
     * @param posY      Vertical position that the car is going to move to
     */
    private void setPosY(double posY) {
        if (posY < this.getCordLowerBound() * 50 ||
                posY > (this.getCordUpperBound() + 1 - this.len) * 50) return;
        this.cordY = convert(posY);
        this.positionY = posY;
        this.setY(posY);
    }

    /**
     * This method gets the X value of the right most coordinate that the car can move to for a
     * horizontal car or the Y value of the bottom most coordinate for a vertical car.
     * @return          Value described above.
     */
    private int getCordUpperBound() {
        return GamePresenter.getCordUpperBound(this);
    }

    /**
     * This method gets the X value of the left most coordinate that the car can move to for a
     * horizontal car or the Y value of the up most coordinate for a vertical car.
     * @return          Value described above.
     */
    private int getCordLowerBound() {
        return GamePresenter.getCordLowerBound(this);
    }

    /**
     * This method sets the position of the Vehicle on the board.
     * @param delta     The delta value of position for this specific car, delta in X for a
     *                  horizontal one and delta in Y for a vertical one.
     */
    public void setPosOnBoard(int delta) {
        if (this.direct == Direction.HORIZONTAL)
            this.setCordX(this.cordX + delta);
        else
            this.setCordY(this.cordY + delta);
    }
}
