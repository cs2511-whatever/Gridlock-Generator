package whatever.gridlock.model;

import whatever.gridlock.Difficulty;
import whatever.gridlock.Direction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Class GameBoard - this class generates a game board with the entities
 * @author      Ruliang Wang
 * @author      Xudong Shi
 */
class GameBoard<E extends Entity> {

    private final List<E> entities;
    private final int[][] matrix;
    private final LinkedHashMap<E, Set<Coordination>> solution;
    private final Solver<E> solver;

    /**
     * This constructor constructs a game board with specified details
     * @param m             Width of the board
     * @param n             Length of the board
     * @param diff          Generated game board
     * @param difficulty    Difficulty of this gameboard
     */
    GameBoard(int m, int n, Generator<E> diff, Difficulty difficulty) {
        this.entities = new ArrayList<>();
        this.matrix = new int[m][n];
        this.solution = new LinkedHashMap<>(diff.generate(this.matrix, entities, difficulty));
        this.solver = new Solver<>();
        new Solver<E>().solve(solution, matrix);
    }

    /**
     * This constructor clones a new game board from an existing game board
     * @param board         Game board to be cloned
     */
    @SuppressWarnings("unchecked")
    GameBoard(GameBoard<E> board) {
        this.entities = new ArrayList<>();
        this.matrix = Arrays.stream(board.matrix)
                .map(int[]::clone)
                .toArray(int[][]::new);
        this.solution = new LinkedHashMap<>();
        for (Map.Entry<E, Set<Coordination>> e : board.solution.entrySet()) {
            Car car = new Car((Car) e.getKey());
            Set<Coordination> cord = new HashSet<>();
            for (Coordination c : e.getValue())
                cord.add(new Coordination(c.getX(), c.getY()));
            this.entities.add((E) car);
            this.solution.put((E) car, cord);
        }
        this.solver = new Solver<>();
    }

    /**
     * This method gets a list of entities on this board
     * in this game, it is cars
     * @return              A list of entities
     */
    List<E> getEntities() {
        return entities;
    }

    /**
     * This method gets hints for current game
     * @return              A HashMap of hints
     */
    Map<E, Coordination> getHint() {
        return solver.solve(solution, matrix);
    }

    /**
     * This method returns the steps required in the solution generated for calculating
     * the score
     * @return The size of solution
     */
    int getSolutionStep () {
        return solution.size();
    }
    /**
     * This method gets a list of integers
     * from the left most coordinate (up most for vertical entities)
     * to right most coordinate the entity can reach
     * @param e             An instance of an entity
     * @return              A list of x or y coordinate the entity can reach
     */
    Set<Integer> getPos(E e) {
        Set<Integer> target = new HashSet<>();
        if (e.getDirect().equals(Direction.HORIZONTAL)) {
            if (e.getHead().getX() < 6) {
                for (int i = e.getHead().getX()+1; i < 6 && matrix[e.getHead().getY()][i] == 0; ++i) {
                    target.add(i);
                }
            }
            if (e.getTail().getX() > 0) {
                for (int i = e.getTail().getX()-1; i >=0 && matrix[e.getTail().getY()][i] == 0; --i) {
                    target.add(i);
                }
            }
            for (int i = e.getTail().getX(); i <= e.getHead().getX(); ++i) {
                target.add(i);
            }
        }
        else {
            if (e.getHead().getY() > 0) {
                for (int i = e.getHead().getY()-1; i >= 0 && matrix[i][e.getHead().getX()] == 0; --i) {
                    target.add(i);
                }
            }
            if (e.getTail().getY() < 6) {
                for (int i = e.getTail().getY()+1; i < 6 && matrix[i][e.getTail().getX()] == 0; ++i) {
                    target.add(i);
                }
            }
            for (int i = e.getHead().getY(); i <= e.getTail().getY(); ++i) {
                target.add(i);
            }
        }
        return target;
    }

    /**
     * This method moves the entity to a new position
     * @param e             The instance of the entity
     * @param distance      Distance that the entity moved
     */
    void setPos(E e, int distance) {
        clearBlock(e);
        if (e.getDirect().equals(Direction.HORIZONTAL)) {
            e.setPos(distance);
            setBlock(e);
        }
        else {
            e.setPos(distance);
            setBlock(e);
        }
        new Solver<E>().solve(solution, matrix);
    }

    /**
     * This method removes an entity from its current position
     * By setting its coordinate to 0
     * @param e             The entity that need to be removed from its current position
     */
    private void clearBlock(E e) {
        for (Coordination c : e.getPos()) {
            matrix[c.getY()][c.getX()] = 0;
        }
    }

    /**
     * This method puts an entity into the game board
     * Obtain the coordinates of the car. Set corresponding matrix
     * element to 1
     * @param e             The entity that to be put on the board
     */
    private void setBlock(E e) {
        for (Coordination c : e.getPos()) {
            matrix[c.getY()][c.getX()] = e.getID();
        }
    }
}
