package whatever.gridlock.model;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;


/**
 * Class AudioPlayer - small custom wrapper of javazoom.jl
 * @author      Fan Yang
 * @author      Zhenyu Yao
 */
public class AudioPlayer {

    private Player player;
    private File music;

    /**
     * Class Constructor - takes the mp3 file to be played as the argument
     * @param file
     */
    public AudioPlayer(File file) {
        this.music = file;
    }

    /**
     * Method that plays the music
     * @throws FileNotFoundException
     * @throws JavaLayerException
     */
    public void play() throws FileNotFoundException, JavaLayerException {
        BufferedInputStream buffer =
                new BufferedInputStream(new FileInputStream(music));
        player = new Player(buffer);
        player.play();
    }
}
