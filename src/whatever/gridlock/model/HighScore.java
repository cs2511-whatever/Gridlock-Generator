package whatever.gridlock.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;


/**
 * Class HighScore - A class of highscore, recording outstanding scores
 * @author      Fan Yang
 * @author      Ruliang Wang
 */
public class HighScore implements Comparator<HighScore> {

    private StringProperty difficulty;
    private StringProperty time;
    private StringProperty step;
    private StringProperty score;

    private static final String filename = "data/high_score.txt";

    /**
     * This constructor constructs a high score instance with specific difficulty,
     * time cost of the game, steps used of the game
     * @param difficulty    Difficulty of the game
     * @param time          Time cost of the game
     * @param step          Step usage of the game
     */
    public HighScore(String difficulty, String time, String step, String score) {
        this.difficulty = new SimpleStringProperty(difficulty);
        this.time = new SimpleStringProperty(time);
        this.step = new SimpleStringProperty(step);
        this.score = new SimpleStringProperty(score);
    }

    /**
     * This method returns the difficulty of the game
     * which this high score belongs to
     * @return  Difficulty of the game that this high score matches
     */
    public StringProperty difficultyProperty() {
        return difficulty;
    }

    /**
     * This method returns the time cost of this round of game
     * @return  Time cost
     */
    public StringProperty timeProperty() {
        return time;
    }

    /**
     * This method returns the step used of the round of game
     * @return  Step of this game
     */
    public StringProperty stepProperty() {
        return step;
    }

    /**
     * This method returns the score of this round of game
     * @return  The score of this game
     */
    public StringProperty scoreProperty() {
        return score;
    }

    /**
     * This method reads high score from input file
     * @return  ArrayList of high scores
     */
    public static ArrayList<HighScore> getHighScoresFromFile() {
        ArrayList<HighScore> scores = new ArrayList<>();
        try {
            File file = new File(filename);
            if (!file.exists()) {
                boolean newFile = file.createNewFile();
                if (!newFile) throw new IOException("File Exists");
            }
            FileReader fr = new FileReader(filename);
            BufferedReader br = new BufferedReader(fr);
            String line;
            String[] splits;
            while ((line = br.readLine()) != null) {
                if (!line.trim().isEmpty()) {
                    splits = line.trim().split(" ");
                    scores.add(new HighScore(splits[0], splits[1], splits[2], splits[3]));
                }
            }
            br.close();
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return scores;
    }

    /**
     * This method adds a new highscore to highscore board
     * write this record to text file
     */
    static void putNewScore(String difficulty, String time, String step, int score) {
        try {
            File file = new File(filename);
            if (!file.exists()) {
                boolean newFile = file.createNewFile();
                if (!newFile) throw new IOException("File Exists");
            }
            FileWriter fw = new FileWriter(filename, true);
            fw.write(difficulty + " " + time + " " + step + " " + score + "\n");
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int compare(HighScore o1, HighScore o2) {
        return Integer.compare(Integer.parseInt(o1.score.getValue()), Integer.parseInt(o2.score.getValue()));
    }
}
