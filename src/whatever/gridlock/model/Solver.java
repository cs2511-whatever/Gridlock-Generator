package whatever.gridlock.model;

import whatever.gridlock.Direction;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;


/**
 * Class: Solver
 * Responsibility: Give hint
 * Collaborator: GeneratorBFS, Coordination, GameBoard
 * @author      Ruliang Wang
 */
class Solver<E extends Entity> {

    /**
     * This function find and return a viable correct step from the solution generated
     * by the generator
     * @param solution      The LinkedHashMap generated from generator, which includes the correct
     *                      solution to the current game
     * @param board         The game board
     * @return              A viable correct next step wrapped in Map
     */
    Map<E, Coordination> solve(LinkedHashMap<E, Set<Coordination>> solution, int[][] board) {
        for (Map.Entry<E, Set<Coordination>> entry : solution.entrySet()) {
            for (Coordination i : entry.getValue()) {
                if (entry.getKey().getHead().equals(i) || entry.getKey().getTail().equals(i)){
                    //System.out.println("Already arrived");
                    break;
                }
                else if (!reachable(entry.getKey(), i, board)) {
                    //System.out.println("Unreachable");
                    break;
                }
                else {
                    //System.out.println("HINT!!!");
                    Map<E, Coordination> hint = new HashMap<>();
                    hint.put(entry.getKey(), i);
                    return hint;
                }
            }
        }
        return null;
    }


    /**
     * This is an helper function which tests if the entity can reach the target
     * coordination
     * @param e             The entity
     * @param target        The target coordination
     * @param board         The board matrix
     * @return              Whether the target is reachable from the entity
     */
    private boolean reachable(E e, Coordination target, int[][] board) {
        if (e.getDirect().equals(Direction.HORIZONTAL)) {
            if (target.getX() > e.getHead().getX()) {
                if (e.getHead().getX() + 1 == target.getX()
                        && board[target.getY()][target.getX()] == 0) return true;
                for (int i = e.getHead().getX() + 1; i <= target.getX(); i++) {
                    if (board[target.getY()][i] != 0) {
                        return false;
                    }
                }
            }
            else {
                if (e.getTail().getX() - 1 == target.getX()
                        && board[target.getY()][target.getX()] == 0) return true;
                for (int i = e.getTail().getX() - 1; i >= target.getX(); i--) {
                    if (board[target.getY()][i] != 0) {
                        return false;
                    }
                }
            }
        }
        else {
            if (target.getY() > e.getHead().getY()) {
                if (e.getTail().getY() + 1 == target.getY()
                        && board[target.getY()][target.getX()] == 0) return true;
                for (int i = e.getTail().getY() + 1; i <= target.getY(); i++) {
                    if (board[i][target.getX()] != 0) {
                        return false;
                    }
                }
            }
            else {
                if (e.getHead().getY() - 1 == target.getY()
                        && board[target.getY()][target.getX()] == 0) return true;
                for (int i = e.getHead().getY() - 1; i >= target.getY(); i--) {
                    if (board[i][target.getX()] != 0) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
