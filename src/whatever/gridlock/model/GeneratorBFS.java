package whatever.gridlock.model;


import whatever.gridlock.Difficulty;
import whatever.gridlock.Direction;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;
import java.util.Set;


/**
 * Class: GeneratorBFS
 * Responsibility: Generate game board interior and cars
 * Collaborator: Car, GameBoard, Coordination, Solver
 * @author      Ruliang Wang
 * @author      Zhenqi Wang
 */
class GeneratorBFS implements Generator<Car> {

    /**
     * This class uses its default constructor due to
     * all its parameters are from GameBoard.
     */
    private int carCounter = 1;
    private LinkedHashMap <Car, Set<Coordination>> solution = new LinkedHashMap<>();


    /**
     * The Choice class act as a data storage unit for
     * possible generation candidates
     * The critical are a series of important tiles. They are
     * the minimal options we have in order to move this car out of the
     * way fro the previous car.
     */
    private class Choice {
        Direction dir;
        String part;
        int size;
        Set<Coordination> critical;

        /**
         * This constructor create a Choice instance
         * @param dir       Direction of the car. Vertical or Horizontal
         * @param part      Whether this tile is the head, the body or the tail of the car
         * @param size      The size of the car
         */
        Choice(Direction dir, String part, int size) {
            this.dir = dir;
            this.part = part;
            this.size = size;
            this.critical = new HashSet<>();
        }
    }


    /**
     * This class is receive empty parameters and fill them with generated content
     * Difficulty is done via controlling how many sub-puzzle is generated and
     * how many cars are generated.
     * @param board         An empty 2-d matrix
     * @param entities      An empty list of cars
     * @param difficulty    The difficulty of the board
     * @return              The solution to this board puzzle
     */
    @Override
    public LinkedHashMap<Car, Set<Coordination>> generate(int[][] board, List<Car> entities, Difficulty difficulty) {
        initMatrix(board.length, board.length, board);
        Car player = generatePlayer(board);
        entities.add(player);
        for (int i = player.getHead().getX() + 1; i < board.length; i++) {
            board[2][i] = -1;
        }
        if (difficulty.equals(Difficulty.EASY)) {
            for (int i = board.length - 1; i >= player.getHead().getX(); i--) {
                if (board[2][i] <= 0) {
                    Coordination critical = new Coordination(i, player.getHead().getY());
                    generateBFS(critical, board, entities, 5);
                }
            }
        } else {
            for (int i = board.length - 1; i >= 0; i--) {
                if (board[2][i] <= 0) {
                    Coordination critical = new Coordination(i, player.getHead().getY());
                    generateBFS(critical, board, entities, 7);
                }
            }
        }
        if (difficulty.equals(Difficulty.HARD)) {
            for (int i = 0; i < board.length; i++) {
                for (int j = 0; j < board.length; j++) {
                    if (board[i][j] <= 0) {
                        Coordination critical = new Coordination(i, j);
                        generateBFS(critical, board, entities, 20);
                    }
                }
            }
        }
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                if (board[i][j] == -1 || board[i][j] == -2) {
                    board[i][j] = 0;
                }
            }
        }
        return solution;
    }


    /**
     * This function is the generation strategy
     * There are two ways of generation I initially discovered. One is very like DFS,
     * which keeps generate on the same 'route' until it is impossible to generate more.
     * The other one is the BFS, which takes target tiles from the same level importance
     * and finish generate them first before moving to next level target tiles.
     * Target tiles are the minimal possible ways of stopping a car from moving to it's
     * destination. There can be one to two target tiles (If this is zero then I will simply
     * not generate on that tile since it is unsolvable) at any time. DFS risks situation where
     * although a single route is fully realized, the player can simply solve the puzzle from
     * the other route. But since BFS generate proceed similar levels first before moving on,
     * such situation can mostly be avoided.
     * @param target        As explained in description
     * @param board         The current board matrix
     * @param entities      The current car list
     * @param ceiling       The highest number of cars allowed for this generation
     */
    private void generateBFS(Coordination target, int[][] board, List<Car> entities, int ceiling) {
        Set<Coordination> result = generateCar(target, board, entities, ceiling);
        if (result == null) return;
        while (!result.isEmpty()) {
            Set<Coordination> newLevel = new HashSet<>();
            for (Coordination i : result) {
                Set<Coordination> x = generateCar(i, board, entities, ceiling);
                if (x != null) {
                    newLevel.addAll(x);
                }
            }
            result = newLevel;
        }
    }


    /**
     * This function generate the player car.
     * It takes the board and generate a car on the third row
     * The player car is always the first car in the car list
     * @param board         The current board matrix
     * @return              A player car
     */
    private Car generatePlayer(int[][] board) {
        Random r = new Random();
        int x = r.nextInt(board.length - 3) + 1;
        Coordination head = new Coordination(x, 2);
        Coordination tail = new Coordination(x-1, 2);
        board[2][x] = 1;
        board[2][x-1] = 1;
        Car player = new Car(1, Direction.HORIZONTAL, 2);
        player.getPos().add(head);
        player.getPos().add(tail);
        Set<Coordination> target = new HashSet<>();
        Coordination destination = new Coordination(board.length - 1, 2);
        target.add(destination);
        solution.put(player, target);
        return player;
    }


    /**
     * Here is how the non-player cars are generated.
     * * The core method of generation is:
     * 1: have a car (This is done initially through generatePlayer, then from generateCar)
     * 2: have target tile(s) to fill so this car can't be moved to its destination
     * 3: For each of these tiles, find options a.k.a 'Choice' we have to generate a car on
     * it.
     * 4: Randomly choose a Choice
     * 5: Generate the car according to that Choice
     * What does this function specifically do?
     * 1: Get all possible ways of generate a car on the target tile
     * 2: Randomly choose one (although a truck is preferred when the tile is on the third
     * line to avoid easy solution)
     * 3: Generate the car
     * 4: Return a set of tile coordination which need to be blocked to prevent this car we
     * just generated to make way for the car that comes before in the same sub-puzzle.
     * @param target        The tile where we need to block
     * @param board         The current board matrix
     * @param entities      The current list of cars
     * @param ceiling       The maximum cars allowed for certain difficulty
     * @return              A set of tile coordination which are the 'target' tiles of this
     *                      car we just generated
     */
    private Set<Coordination> generateCar(Coordination target, int[][] board, List<Car> entities, int ceiling) {
        if (board[target.getY()][target.getX()] > 0 || carCounter > ceiling) return null;
        boolean extra = false;
        if (board[target.getY()][target.getX()] == 0) extra = true;
        board[target.getY()][target.getX()] = 0;
        Random r = new Random();
        List<Choice> choiceSet = new ArrayList<>();
        boolean flag = true;
        Direction dir = Direction.VERTICAL;
        for (int i = 2; i <= 3; i++) {
            if (target.getY()-i >= 0 && board[target.getY()-i][target.getX()] == 0) {
                for (int j = target.getY()-1; j >= target.getY()-i; j--) {
                    if (board[j][target.getX()] != 0) {
                        flag = false;
                        break;
                    }
                }
            }
            else {
                flag = false;
            }
            if (flag) {
                Coordination critical = new Coordination(target.getX(), target.getY()-i);
                if (validate(board, target, "Tail", i, dir)) updateChoice(choiceSet, dir, "Tail", i, critical);
                if (validate(board, target, "Head", i, dir)) updateChoice(choiceSet, dir, "Head", i, critical);
            }

            flag = true;
            if (target.getY()+i < board.length && board[target.getY()+i][target.getX()] == 0) {
                for (int j = target.getY()+1; j <= target.getY()+i; j++) {
                    if (board[j][target.getX()] != 0) {
                        flag = false;
                        break;
                    }
                }
            }
            else {
                flag = false;
            }
            if (flag) {
                Coordination critical = new Coordination(target.getX(), target.getY()+i);
                if (validate(board, target, "Head", i, dir)) updateChoice(choiceSet, dir, "Head", i, critical);
                if (validate(board, target, "Tail", i, dir)) updateChoice(choiceSet, dir, "Tail", i, critical);
            }

            if (i == 3) {
                flag = true;
                if (target.getY() + 3 < board.length && board[target.getY() + 3][target.getX()] == 0) {
                    for (int j = target.getY()+1; j <= target.getY() + 3; j++) {
                        if (board[j][target.getX()] != 0) {
                            flag = false;
                            break;
                        }
                    }
                } else {
                    flag = false;
                }
                if (flag) {
                    Coordination critical = new Coordination(target.getX(), target.getY() + 3);
                    if (validate(board, target, "Body", i, dir)) updateChoice(choiceSet, dir, "Body", i, critical);
                }

                flag = true;
                if (target.getY() - 3 >= 0 && board[target.getY() - 3][target.getX()] == 0) {
                    for (int j = target.getY()-1; j >= target.getY() - 3; j--) {
                        if (board[j][target.getX()] != 0) {
                            flag = false;
                            break;
                        }
                    }
                } else {
                    flag = false;
                }
                if (flag) {
                    Coordination critical = new Coordination(target.getX(), target.getY() - 3);
                    if (validate(board, target, "Body", i, dir)) updateChoice(choiceSet, dir, "Body", i, critical);
                }
            }
        }
        flag = true;
        if (target.getY() != 2) {
            dir = Direction.HORIZONTAL;
            for (int i = 2; i <= 3; ++i) {
                if (target.getX()+i < board[0].length && board[target.getY()][target.getX()+i] == 0) {
                    for (int j = target.getX()+1; j <= target.getX()+i; j++) {
                        if (board[target.getY()][j] != 0) {
                            flag = false;
                            break;
                        }
                    }
                }
                else {
                    flag = false;
                }
                if (flag) {
                    Coordination critical = new Coordination(target.getX()+i, target.getY());
                    if (validate(board, target, "Head", i, dir)) updateChoice(choiceSet, dir, "Head", i, critical);
                    if (validate(board, target, "Tail", i, dir)) updateChoice(choiceSet, dir, "Tail", i, critical);
                }

                flag = true;
                if (target.getX()-i >= 0 && board[target.getY()][target.getX()-i] == 0) {
                    for (int j = target.getX()-1; j >= target.getX()-i; j--) {
                        if (board[target.getY()][j] != 0) {
                            flag = false;
                            break;
                        }
                    }
                }
                else {
                    flag = false;
                }
                if (flag) {
                    Coordination critical = new Coordination(target.getX()-i, target.getY());
                    if (validate(board, target, "Head", i, dir)) updateChoice(choiceSet, dir, "Head", i, critical);
                    if (validate(board, target, "Tail", i, dir)) updateChoice(choiceSet, dir, "Tail", i, critical);
                }

                if (i == 3) {
                    flag = true;
                    if (target.getX() + 3 < board.length && board[target.getY()][target.getX()+3] == 0) {
                        for (int j = target.getX()+1; j <= target.getX() + 3; j++) {
                            if (board[target.getY()][j] != 0) {
                                flag = false;
                                break;
                            }
                        }
                    } else {
                        flag = false;
                    }
                    if (flag) {
                        Coordination critical = new Coordination(target.getX() + 3, target.getY());
                        if (validate(board, target, "Body", i, dir)) updateChoice(choiceSet, dir, "Body", i, critical);
                    }

                    flag = true;
                    if (target.getX() - 3 >= 0 && board[target.getY()][target.getX()-3] == 0) {
                        for (int j = target.getX()-1; j <= target.getX() - 3; j--) {
                            if (board[target.getY()][j] != 0) {
                                flag = false;
                            }
                        }
                    } else {
                        flag = false;
                    }
                    if (flag) {
                        Coordination critical = new Coordination(target.getX() - 3, target.getY());
                        if (validate(board, target, "Body", i, dir)) updateChoice(choiceSet, dir, "Body", i, critical);
                    }
                }
            }
        }
        if (choiceSet.size() == 0) {
            board[target.getY()][target.getX()] = -1;
            return null;
        }
        int index = r.nextInt(choiceSet.size());
        Choice result = choiceSet.get(index);
        if (target.getY() == 2) {
            for (Choice i : choiceSet) {
                if (i.size == 3) {
                    result = i;
                    break;
                }
            }
        }
        Car newCar;
        carCounter++;
        if (result.size == 2) {
            newCar = generateNormalCar(result.part,result.dir,target,board);
        }
        else {
            newCar = generateTruck(result.part, result.dir,target,board);
        }
        entities.add(newCar);
        for (Coordination critical : result.critical) {
            if (result.dir.equals(Direction.VERTICAL)) {
                if (critical.getY() < newCar.getHead().getY()) {
                    block(newCar.getHead(), critical, board);
                } else {
                    block(newCar.getTail(), critical, board);
                }
            } else {
                if (critical.getX() > newCar.getHead().getX()) {
                    block(newCar.getHead(), critical, board);
                } else {
                    block(newCar.getTail(), critical, board);
                }
            }
        }
        if (!extra) solution.put(newCar, result.critical);
        return result.critical;
    }


    /**
     * This function is invoked by the generateCar method to generate a size 3 car based
     * on the parameters passed in.
     * @param part          The identity of the target tile
     * @param dir           The direction of the car
     * @param target        The target tile coordination
     * @param board         The current board matrix
     * @return              A size 3 car
     */
    private Car generateNormalCar(String part, Direction dir, Coordination target, int[][] board){
        Car newCar;
        Coordination head;
        Coordination tail;
        switch (part) {
            case "Head":
                head = new Coordination(target.getX(), target.getY());
                if (dir.equals(Direction.HORIZONTAL)) tail = new Coordination(target.getX() - 1, target.getY());
                else
                    tail = new Coordination(target.getX(), target.getY() + 1);
                break;
            default:
                tail = new Coordination(target.getX(), target.getY());
                if (dir.equals(Direction.HORIZONTAL))  head = new Coordination(target.getX() + 1, target.getY());
                else
                    head = new Coordination(target.getX(), target.getY() - 1);
                break;
        }
        newCar = new Car(carCounter,dir , 2);
        newCar.getPos().add(head);
        newCar.getPos().add(tail);
        board[head.getY()][head.getX()] = carCounter;
        board[tail.getY()][tail.getX()] = carCounter;
        return newCar;
    }


    /**
     * This function is invoked by the generateCar method to generate a size 2 car based
     * on the parameters passed in.
     * @param part          The identity of the target tile
     * @param dir           The direction of the car
     * @param target        The target tile coordination
     * @param board         The current board matrix
     * @return              A size 2 car
     */
    private Car generateTruck(String part, Direction dir, Coordination target, int[][] board){
        Car newCar;
        Coordination head;
        Coordination tail;
        Coordination body;
        switch (part) {
            case "Head":
                head = new Coordination(target.getX(), target.getY());
                if (dir.equals(Direction.HORIZONTAL)) {
                    body = new Coordination(target.getX() - 1, target.getY());
                    tail = new Coordination(target.getX() - 2, target.getY());
                } else {
                    body = new Coordination(target.getX(), target.getY() + 1);
                    tail = new Coordination(target.getX(), target.getY() + 2);
                }
                break;
            case "Body":
                if (dir.equals(Direction.HORIZONTAL)) {
                    body = new Coordination(target.getX(), target.getY());
                    head = new Coordination(target.getX() + 1, target.getY());
                    tail = new Coordination(target.getX() - 1, target.getY());
                } else {
                    body = new Coordination(target.getX(), target.getY());
                    head = new Coordination(target.getX(), target.getY() - 1);
                    tail = new Coordination(target.getX(), target.getY() + 1);
                }
                break;
            default:
                tail = new Coordination(target.getX(), target.getY());
                if (dir.equals(Direction.HORIZONTAL)) {
                    body = new Coordination(target.getX() + 1, target.getY());
                    head = new Coordination(target.getX() + 2, target.getY());
                } else {
                    body = new Coordination(target.getX(), target.getY() - 1);
                    head = new Coordination(target.getX(), target.getY() - 2);
                }
                break;
        }
        newCar = new Car(carCounter, dir, 3);
        newCar.getPos().add(head);
        newCar.getPos().add(body);
        newCar.getPos().add(tail);
        board[head.getY()][head.getX()] = carCounter;
        board[body.getY()][body.getX()] = carCounter;
        board[tail.getY()][tail.getX()] = carCounter;
        return newCar;
    }


    /**
     * An helper function which initialise the matrix passed in
     * @param col           The number of column
     * @param row           The number of rows
     * @param board         The uninitialized board matrix
     */
    private void initMatrix(int col, int row, int[][] board) {
        for (int i = 0; i < row; ++i) {
            for (int j = 0; j < col; ++j) {
                board[i][j] = 0;
            }
        }
    }


    /**
     * This function update Choice entry in the choiceSet used in generateCars
     * If similar Choice already exists, update its potential targets, else add
     * new option.
     * @param choiceSet     The list of Choices
     * @param dir           Direction of the Choice
     * @param part          Identity of the tile
     * @param size          Size of the potential car
     * @param co            The target coordination
     */
    private void updateChoice(List<Choice> choiceSet, Direction dir, String part, int size, Coordination co) {
        boolean test = true;
        for(Choice c:choiceSet) {
            if (c.dir.equals(dir) && c.part.equals(part) && c.size == size) {
                c.critical.add(co);
                test = false;
            }
        }
        if (test) {
            Choice c = new Choice(dir, part, size);
            c.critical.add(co);
            choiceSet.add(c);
        }
    }


    /**
     * This is an helper function which checks if placing a car in the current tile is
     * possible
     * @param board         The current board matrix
     * @param target        The tile where this car is going to be placed
     * @param part          The supposed identity of the tile
     * @param size          The supposed size of the potential car
     * @param direction     The direction of the tile
     * @return              Whether it is possible to place a car here
     */
    private boolean validate(int[][] board, Coordination target, String part, int size, Direction direction) {
        if (direction.equals(Direction.VERTICAL)) {
            switch (part) {
                case "Head":
                    return target.getY() + size - 1 < board.length &&
                            board[target.getY() + size - 1][target.getX()] == 0 &&
                            board[target.getY() + size - 2][target.getX()] == 0;
                case "Body":
                    return target.getY() - 1 >= 0 && target.getY() + 1 < board.length &&
                            board[target.getY() + 1][target.getX()] == 0 &&
                            board[target.getY() - 1][target.getX()] == 0;
                default:
                    return target.getY() - size + 1 >= 0 && target.getY() - size + 2 >= 0 &&
                            board[target.getY() - size + 1][target.getX()] == 0 &&
                            board[target.getY() - size + 2][target.getX()] == 0;
            }
        }
        else {
            switch (part) {
                case "Tail":
                    return target.getX() + size - 1 < board.length && target.getX() + size - 2 < board.length &&
                            board[target.getY()][target.getX() + size - 1] == 0 &&
                            board[target.getY()][target.getX() + size - 2] == 0;
                case "Body":
                    return target.getX() - 1 >= 0 && target.getX() + 1 < board.length &&
                            board[target.getY()][target.getX() + 1] == 0 &&
                            board[target.getY()][target.getX() - 1] == 0;
                default:
                    return target.getX() - size + 1 >= 0 && target.getX() - size + 2 >= 0 &&
                            board[target.getY()][target.getX() - size + 1] == 0 &&
                            board[target.getY()][target.getX() - size + 2] == 0;
            }
        }
    }


    /**
     * This is another helper function which block the path between two coordination
     * The reason behind this is to disallow car generation on tiles between a car and
     * its target tile.
     * Sure this can be done and the car can potentially still be lifted out of its lock.
     * However if the generation process take the target tile of a car as its own target tile,
     * or take a tile that is after it as a target tile, a live lock is very likely to happen.
     * i.e: Car A, which is on route Y need to move to tile a to allow route X to be solved,
     * yet Car B, which is on route X also needs to move to the same tile, or pass this tile
     * to allow route Y to be solved.
     * @param from          The tile where we are from
     * @param to            The tile where we are going to
     * @param board         The current board
     */
    private void block(Coordination from, Coordination to, int[][] board) {
        if (from.getX() == to.getX()) {
            if (from.getY() > to.getY()) {
                for (int i = to.getY(); i < from.getY(); i++) {
                    board[i][to.getX()] = -1;
                }
            }
            else {
                for (int i = from.getY() + 1; i <= to.getY(); i++) {
                    board[i][to.getX()] = -1;
                }
            }
        }
        else {
            if (from.getX() > to.getX()) {
                for (int i = to.getX(); i < from.getX(); i++) {
                    board[to.getY()][i] = -1;
                }
            }
            else {
                for (int i = from.getX() + 1; i <= to.getX(); i++) {
                    board[to.getY()][i] = -1;
                }
            }
        }
    }
}
