package whatever.gridlock.model;


/**
 * Class Coordination - represents a coordination for Car with specific x-axis and y-axis values
 * @author      Ruliang Wang
 * @author      Xudong Shi
 * @author      Zhenqi Wang
 */
public class Coordination {
    private int x;
    private int y;

    /**
     * This constructor constructs a coordinate with specific x and y axis
     * A part of a car
     * @param x     The horizontal position of the part of the car
     * @param y     The vertical position of the part of the car
     */
    Coordination(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * This method returns x axis of this coordinate
     * @return      The x axis value of the matrix
     */
    int getX() {
        return x;
    }

    /**
     * This method returns y axis of this coordinate
     * @return      The y axis value of the matrix
     */
    int getY() {
        return y;
    }

    /**
     * This method changes x axis of this coordinate
     * if this changed, car move horizontally
     * @param x     New x axis value of the coordinate
     */
    void setX(int x) {
        this.x = x;
    }

    /**
     * This method changes y axis of this coordinate
     * Y is set means car is moved vertically
     * @param y     New Y axis value of the coordinate
     */
    void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "[" + x + "," + y + "]";
    }

    @Override
    public boolean equals(Object obj) {
        return ((Coordination) obj).getX() == x && ((Coordination) obj).getY() == y;
    }
}
