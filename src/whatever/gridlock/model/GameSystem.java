package whatever.gridlock.model;

import whatever.gridlock.Difficulty;
import whatever.gridlock.Direction;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Class GameSystem - the class that controls the game operation
 * @author      Ruliang Wang
 * @author      Zhenyu Yao
 * @author      Xudong Shi
 */
public class GameSystem {

    private final Difficulty difficulty;
    private GameBoard<Car> board;
    private final GameBoard<Car> initBoard;

    /**
     * This constructor constructs a game system with specified difficulty
     * @param           difficulty The difficulty to generate
     */
    public GameSystem(Difficulty difficulty) {
        this.difficulty = difficulty;
        this.board = new GameBoard<>(6, 6, new GeneratorBFS(), difficulty);
        this.initBoard = new GameBoard<>(board);
    }

    /**
     * This method gets all entities on the gameboard
     * which should be vehicles in this game
     * @return          List of entities on this gameboard
     */
    public List<Car> getEntities() {
        return board.getEntities();
    }

    /**
     * This method gets the positions of a car
     * @param e         The car to get pos from
     * @return          Set of integers, for horizontal cars it's a set of X coordinates,
     *                  for vertical cars it's a set of Y coordinates
     */
    public Set<Integer> getPos(Car e) { // proposed change: rename as gerCurrentPosition
        return board.getPos(e);
    }

    /**
     * This method changes the position of the car
     * @param e         A car instance
     * @param distance  Distance the car will move
     */
    public void setPos(Car e, int distance) { // proposed change: rename as setNewPosition
        board.setPos(e, distance);
    }

    /**
     * This method sets a score with specific difficulty of the game,
     * time cost, step usage, hint of this round of game
     */
    public void setScore(String difficulty, String time, String step, int hint){
        int baseMark = 0;
        if (difficulty.equals("rookie")) baseMark = 10;
        if (difficulty.equals("normal")) baseMark = 15;
        if (difficulty.equals("veteran")) baseMark = 20;
        int timeMark = Integer.parseInt(time.split(":")[1])
                + Integer.parseInt(time.split(":")[0]) * 60;
        int hintStep = board.getSolutionStep();
        int stepMark = Integer.parseInt(step);
        int processedStepMark;
        if (stepMark - hintStep < 0) {
            processedStepMark = 0;
        }
        else {
            processedStepMark = stepMark - hintStep;
        }
        int finalScore = baseMark - timeMark/10 - hint - processedStepMark;
        if (finalScore < 0) finalScore = 0;
        HighScore.putNewScore(difficulty, time, step, finalScore);
    }

    /**
     * This method returns the difficulty of current board
     * @return          The difficulty of the instance
     */
    public Difficulty getDifficulty(){
        return difficulty;
    }

    /**
     * This method returns the hint of this game
     * @return          A HashMap of hint of the game
     */
    public Map<Car, Integer> getHint() {
        Map<Car, Coordination> hint = board.getHint();
        Car car = null;
        Coordination cord = null;
        if (hint == null) {
            return new HashMap<>();
        }
        if (hint.entrySet().isEmpty()) {
            throw new RuntimeException("hint error");
        }
        for (Map.Entry<Car, Coordination> e : hint.entrySet()) {
            car = e.getKey();
            cord = e.getValue();
        }
        int delta;
        if (car.getDirect().equals(Direction.HORIZONTAL)) {
            delta = cord.getX() - car.getTopLeftX();
            if (delta > 0) delta -= (car.getSize() - 1);
        }
        else {
            delta = cord.getY() - car.getTopLeftY();
            if (delta > 0) delta -= (car.getSize() - 1);
        }
        if (delta == 0) return new HashMap<>();
        setPos(car, delta);
        Map<Car, Integer> ret = new HashMap<>();
        ret.put(car, delta);
        return ret;
    }

    /**
     * This method restores current game to its initial condition
     */
    public void reset() {
        board = new GameBoard<>(initBoard);
    }
}
