package whatever.gridlock.model;

import whatever.gridlock.Difficulty;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;


/**
 * Interface Generator - an interface for the game board generator.
 * @author      Ruliang Wang
 * @author      Zhenqi Wang
 */
public interface Generator<E extends Entity> {

    /**
     * This method returns a generated game board
     * @param board         An empty game board array
     * @param entities      A list of entities to be added to the board
     * @param difficulty    The difficulty of the board
     * @return              A generated game board
     */
    LinkedHashMap<E, Set<Coordination>> generate(int[][] board, List<E> entities, Difficulty difficulty);
}
