package whatever.gridlock.model;

import whatever.gridlock.Direction;

import java.util.List;
import java.util.ArrayList;


/**
 * Class Car - an implementation of entity, represents the cars on the game board
 * @author      Ruliang Wang
 * @author      Zhenqi Wang
 */
public class Car implements Entity {

    private int id;
    private List<Coordination> position;
    private Direction direct;
    private int size;

    /**
     * This constructor constructs a car with its id, heading and size
     * @param id        The id of the car
     * @param direct    The direction where the car is heading - either "Horizontal" or "Vertical"
     * @param size      The size of a car
     */
    Car(int id, Direction direct, int size){
        this.position = new ArrayList<>();
        this.id = id;
        this.direct = direct;
        this.size = size;
    }

    /**
     * This constructor copies details of an existing car into a new car instance
     * @param car       The car which going to be copied
     */
    Car(Car car) {
        this.position = new ArrayList<>();
        for (Coordination cord : car.position)
            this.position.add(new Coordination(cord.getX(), cord.getY()));
        this.id = car.id;
        this.direct = car.direct;
        this.size = car.size;
    }

    @Override
    public List<Coordination> getPos() {
        return position;
    }

    @Override
    public Direction getDirect() {
        return direct;
    }

    @Override
    public Coordination getHead() {
        return position.get(0);
    }

    @Override
    public Coordination getTail() {
        return position.get(position.size() - 1);
    }

    @Override
    public void setPos(int distance) {
        for (Coordination i : position) {
            if (direct.equals(Direction.HORIZONTAL)) i.setX(i.getX()+distance);
            if (direct.equals(Direction.VERTICAL)) i.setY(i.getY()+distance);
        }
    }

    @Override
    public boolean isPlayer() {
        return id == 1;
    }

    @Override
    public int getID() {
        return id;
    }

    /**
     * This method gets the top left x coordination of a Car
     * @return          The top left x coordination of a Car
     */
    public int getTopLeftX() {
        if (direct.equals(Direction.VERTICAL)) return getHead().getX();
        return getTail().getX();
    }

    /**
     * This method gets the top left y coordination of a Car
     * @return          The top left y coordination of a Car
     */
    public int getTopLeftY() {
        if (direct.equals(Direction.VERTICAL)) return getHead().getY();
        return getTail().getY();
    }

    /**
     * This method gets the size of a Car
     * @return          The size of a Car
     */
    public int getSize() {
        return size;
    }
}
