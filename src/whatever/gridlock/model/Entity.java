package whatever.gridlock.model;

import whatever.gridlock.Direction;

import java.util.List;


/**
 * Interface Entity - an interface for an object on the game board
 * @author      Ruliang Wang
 * @author      Zhenqi Wang
 */
public interface Entity {

    /**
     * This method gets the coordination of an entity
     * @return  the coordination of an entity
     */
    List<Coordination> getPos();

    /**
     * This method gets the direction of an entity
     * @return  the direction of an entity
     */
    Direction getDirect();

    /**
     * This method gets the coordination of the head of an entity
     * @return  the coordination of the head of an entity
     */
    Coordination getHead();

    /**
     * This method gets the coordination of the tail of an entity
     * @return  the coordination of the tail of an entity
     */
    Coordination getTail();

    /**
     * This method gets the id of an entity
     * @return  the id of an entity
     */
    int getID();

    /**
     * This method sets the position of an entity after movement
     * @param   distance the distance to move for the entity
     */
    void setPos(int distance);

    /**
     * This method gets if this entity is a player car
     * @return  true if this is the player car, false otherwise
     */
    boolean isPlayer();
}
