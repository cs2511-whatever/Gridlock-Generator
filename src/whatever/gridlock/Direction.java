package whatever.gridlock;


/**
 * Enumerator Direction - representation of directions for vehicles
 * @author      Zhenyu Yao
 */
public enum Direction {
    HORIZONTAL, VERTICAL
}
