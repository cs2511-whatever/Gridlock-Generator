package whatever.gridlock.presenter;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import whatever.gridlock.Main;


/**
 * Class HelpPagePresenter - presenter of the help page
 * @author      Fan Yang
 * @author      Zhenqi Wang
 */
public class HelpPagePresenter {

    @FXML
    TextArea help_text;

    /**
     * This method fills the text for help
     */
    @FXML
    private void initialize() {
        help_text.setEditable(false);
        help_text.setText("Welcome to the gridLock game!\n" +
                "The following instruction will teach you the basic\n" +
                "aspects of this game.\n" +
                "\n" +
                "1.Get Started\n" +
                "To start game, click on \"Start Game\", then choose\n" +
                "the difficulty level you want, and then you're good\n" +
                "to go!\n" +
                "\n" +
                "2.Rules\n" +
                "The objective of this game is to direct the red car\n" +
                "to the right most tile by move all obstacles out of\n" +
                "the way. Notice that a car can only move back and\n" +
                "forth in the direction it's heading. The system will\n" +
                "start counting player step once the first car is\n" +
                "moved, and the timer will start as well.\n" +
                "\n" +
                "3.Score\n" +
                "The score you get will be based on difficulty, the\n" +
                "number of steps and timing. Below are the base\n" +
                "score you can get for finishing each\n" +
                "difficulty.\n" +
                "Easy - 10pts\n" +
                "Normal - 15pts\n" +
                "Hard - 20pts\n" +
                "Timing and steps will be taken into consideration\n" +
                "as well as the difficulty.\n" +
                "\n" +
                "The score board lists the games in descending\n" +
                "order of the final score and their respective\n" +
                "difficulty.\n" +
                "\n" +
                "Good luck and have fun :)");
    }

    /**
     * This method swaps current game scene to home page
     */
    @FXML
    public void backHome() {
        Main.toHomePage();
    }
}
