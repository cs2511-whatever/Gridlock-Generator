package whatever.gridlock.presenter;

import javafx.fxml.FXML;

import whatever.gridlock.Main;


/**
 * Class MenuPresenter - presenter of the game menu
 * @author      Zhenyu Yao
 * @author      Zhenqi Wang
 */
public class MenuPresenter {

    /**
     * A method that starts game and go to difficulty selection
     */
    @FXML
    protected void startGame() {
        Main.toDifficultyPage();
    }

    /**
     * A method that exits the game
     */
    @FXML
    protected void exitGame() {
        Main.exitProgram();
    }

    /**
     * A method that navigates to high score board
     */
    @FXML
    protected void scoreBoard() {
        Main.toScoreBoard();
    }

    /**
     * A method that navigates to help page
     */
    @FXML
    protected void helpPage() {
        Main.toHelpPage();
    }
}
