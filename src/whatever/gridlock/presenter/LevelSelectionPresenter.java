package whatever.gridlock.presenter;

import javafx.fxml.FXML;

import whatever.gridlock.Difficulty;
import whatever.gridlock.Main;


/**
 * Class LevelSelectionPresenter - presenter for game difficulty selection page
 * @author      Zhenyu Yao
 * @author      Fan Yang
 * @author      Zhenqi Wang
 */
public class LevelSelectionPresenter {

    /**
     * A method that starts the game at rookie difficulty
     */
    @FXML
    protected void rookieGame(){
        Main.startGame(Difficulty.EASY);
    }

    /**
     * A method that starts the game at normal difficulty
     */
    @FXML
    protected void normalGame(){
        Main.startGame(Difficulty.NORMAL);
    }

    /**
     * A method that starts the game at veteran difficulty
     */
    @FXML
    protected void veteranGame(){
        Main.startGame(Difficulty.HARD);
    }

    /**
     * A method that returns to the menu page
     */
    @FXML
    protected void toHomePage() {
        Main.toHomePage();
    }
}
