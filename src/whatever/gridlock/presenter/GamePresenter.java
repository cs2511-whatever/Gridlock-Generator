package whatever.gridlock.presenter;

import java.util.Collections;
import java.util.Map;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.ResourceBundle;
import java.net.URL;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;

import whatever.gridlock.Difficulty;
import whatever.gridlock.Direction;
import whatever.gridlock.Main;
import whatever.gridlock.model.Car;
import whatever.gridlock.view.Vehicle;


/**
 * Class GamePresenter - presenter of the gridlock game
 * @author      Zhenyu Yao
 * @author      Fan Yang
 * @author      Zhenqi Wang
 */
public class GamePresenter implements Initializable {

    private static GamePresenter game;

    private Timer timer;
    private int hintUsedTimes;
    private Difficulty difficulty;
    private Map<Vehicle, Car> vcBundle;

    @FXML
    private Pane gamePane;
    @FXML
    private Label timeUsed;
    @FXML
    private Label stepDone;

    private static String score;
    private boolean isStart;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        GamePresenter.game = this;
        this.vcBundle = new HashMap<>();
        this.difficulty = Main.getGameSystem().getDifficulty();
        initCars();
        initTime();
        initStep();
        initHint();
        countTime();
    }

    /**
     * Method that initialises a list of cars on the game board
     */
    private void initCars() {
        gamePane.getChildren().clear();
        for (Car c : Main.getGameSystem().getEntities()) {
            Vehicle v;
            if (c.isPlayer())
                v = Vehicle.redCar(c.getTopLeftX(), getUrl(true, c.getSize(), c.getDirect()));
            else
                v = Vehicle.normalCar(c.getTopLeftX(), c.getTopLeftY(), c.getSize(),
                        c.getDirect(), getUrl(false, c.getSize(), c.getDirect()));
            vcBundle.put(v, c);
            gamePane.getChildren().add(v);
        }
    }

    /**
     * Method that initialises the timer to 00:00
     */
    private void initTime(){
        timeUsed.setText("00:00");
    }

    /**
     * Method that initialises number of steps to 0
     */
    private void initStep(){
        stepDone.setText("000");
    }

    /**
     * Method that initialises times of hint function used
     */
    private void initHint() {
        hintUsedTimes = 0;
    }

    /**
     * Method that implements a timer function to count time of the game
     */
    private void countTime() {
        if (timer != null)
            timer.cancel();
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(() -> {
                    if (!isStart) {
                        isStart = true;
                    } else {
                        String timeString = "00:" + timeUsed.getText();
                        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
                        LocalTime time = LocalTime.parse(timeString, dtf);
                        String resultString = time.plusSeconds(1).format(dtf);
                        timeUsed.setText(resultString.substring(3));
                    }
                });
            }
        }, 0, 1000);
    }

    /**
     * Method that increases the number of steps by one upon movement
     */
    @FXML
    private void countStep() {
        stepDone.setText(String.format("%03d", Integer.parseInt(stepDone.getText()) + 1));
        GamePresenter.score = String.format("%03d", Integer.parseInt(stepDone.getText()) + 1);
    }

    /**
     * Method that resets current game to its initial condition
     */
    @FXML
    private void resetGame() {
        Main.getGameSystem().reset();
        this.vcBundle = new HashMap<>();
        initCars();
        initTime();
        initStep();
        initHint();
        countTime();
    }

    /**
     * Method that shows hint by moving cars
     */
    @FXML
    private void hintNext() {
        Map<Car, Integer> nextStep = Main.getGameSystem().getHint();
        if (nextStep.entrySet().isEmpty()) return;
        Car car = null;
        int delta = 0;
        for (Map.Entry<Car, Integer> e : nextStep.entrySet()) {
            car = e.getKey();
            delta = e.getValue();
        }
        if (delta == 0) return;
        stepStep();
        hintUsedTimes++;
        Vehicle v = null;
        for (Map.Entry<Vehicle, Car> e : vcBundle.entrySet())
            if (car.equals(e.getValue()))
                v = e.getKey();
        if (v == null) throw new RuntimeException("ERROR");
        v.setPosOnBoard(delta);
    }

    /**
     * Method that exits the game
     */
    @FXML
    protected void quitGame() {
        Alert alert = new Alert(AlertType.CONFIRMATION, "Quit Game?", ButtonType.YES, ButtonType.NO);
        alert.showAndWait();
        if (alert.getResult() == ButtonType.YES) {
            Main.toHomePage();
        }
    }

    /**
     * Method that gets a random url for a car image
     * @param isUser    whether this is user car
     * @param size      the size of the vehicle
     * @param direct    the direction of the vehicle
     * @return          a url for the required car
     */
    private static String getUrl(boolean isUser, int size, Direction direct) {
        if (isUser) {
            if (direct.equals(Direction.VERTICAL)) return "assets/usercar_up.png";
            else if (direct.equals(Direction.HORIZONTAL)) return "assets/usercar_right.png";
        }
        double d = Math.random();
        int a = (int) (d * 100);
        if (direct.equals(Direction.VERTICAL)) {
            if (size == 2) {
                int i = a % 8;
                if (i == 0) return "assets/car1_up.png";
                if (i == 1) return "assets/car1_down.png";
                if (i == 2) return "assets/car2_up.png";
                if (i == 3) return "assets/car2_down.png";
                if (i == 4) return "assets/car3_up.png";
                if (i == 5) return "assets/car3_down.png";
                if (i == 6) return "assets/car4_up.png";
                if (i == 7) return "assets/car4_down.png";
            } else {
                int i = a % 4;
                if (i == 0) return "assets/truck1_up.png";
                if (i == 1) return "assets/truck1_down.png";
                if (i == 2) return "assets/truck2_up.png";
                if (i == 3) return "assets/truck2_down.png";
            }
        } else if (direct.equals(Direction.HORIZONTAL)) {
            if (size == 2) {
                int i = a % 8;
                if (i == 0) return "assets/car1_left.png";
                if (i == 1) return "assets/car1_right.png";
                if (i == 2) return "assets/car2_left.png";
                if (i == 3) return "assets/car2_right.png";
                if (i == 4) return "assets/car3_left.png";
                if (i == 5) return "assets/car3_right.png";
                if (i == 6) return "assets/car4_left.png";
                if (i == 7) return "assets/car4_right.png";
            } else {
                int i = a % 4;
                if (i == 0) return "assets/truck1_left.png";
                if (i == 1) return "assets/truck1_right.png";
                if (i == 2) return "assets/truck2_left.png";
                if (i == 3) return "assets/truck2_right.png";
            }
        }
        return "";
    }

    /**
     * Method that counts step after each move
     */
    public static void stepStep() {
        GamePresenter.game.countStep();
    }

    /**
     * Method that shows the "you win" message after finishing game
     */
    public static void winGame() {
        GamePresenter.game.timer.cancel();
        Alert alert = new Alert(AlertType.INFORMATION, "You Win!", ButtonType.OK);
        alert.showAndWait();

        Main.getGameSystem().setScore(GamePresenter.game.difficulty.toString(),
                GamePresenter.game.timeUsed.getText(), score, GamePresenter.game.hintUsedTimes);
        Main.toDifficultyPage();
    }

    /**
     * Method that gets the upper bound of the vehicle
     * @param v     A vehicle instance
     * @return      The upper bound of this vehicle
     */
    public static int getCordUpperBound(Vehicle v) {
        Car c = GamePresenter.game.vcBundle.get(v);
        return Collections.max(Main.getGameSystem().getPos(c));
    }

    /**
     * Method that gets the lower bound of the vehicle
     * @param v     A vehicle instance
     * @return      The lower bound of this vehicle
     */
    public static int getCordLowerBound(Vehicle v) {
        Car c = GamePresenter.game.vcBundle.get(v);
        return Collections.min(Main.getGameSystem().getPos(c));
    }

    /**
     * Method that sets the new positions of a vehicle
     * @param v     A vehicle instance
     * @param delta The change of position
     */
    public static void setNewPos(Vehicle v, int delta) {
        Car c = GamePresenter.game.vcBundle.get(v);
        Main.getGameSystem().setPos(c, delta);
    }
}
