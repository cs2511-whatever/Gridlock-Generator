package whatever.gridlock.presenter;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import whatever.gridlock.Main;
import whatever.gridlock.model.HighScore;

import java.util.ArrayList;


/**
 * Class ScoreBoardPresenter - presenter of the high score board
 * @author      Fan Yang
 * @author      Xudong Shi
 */
public class ScoreBoardPresenter {

    @FXML
    private TableView<HighScore> high_table;
    @FXML
    private TableColumn<HighScore, String> diff_column;
    @FXML
    private TableColumn<HighScore, String> time_column;
    @FXML
    private TableColumn<HighScore, String> step_column;
    @FXML
    private TableColumn<HighScore, String> score_column;

    /**
     * This method initializes cell value with difficulty, time, step properties
     */
    @FXML
    private void initialize() {
        diff_column.setCellValueFactory(cellData -> cellData.getValue().difficultyProperty());
        time_column.setCellValueFactory(cellData -> cellData.getValue().timeProperty());
        step_column.setCellValueFactory(cellData -> cellData.getValue().stepProperty());
        score_column.setCellValueFactory(cellData -> cellData.getValue().scoreProperty());
        loadData();
    }

    /**
     * This method fills data to high score
     */
    private void loadData() {
        ObservableList<HighScore> scoreData = FXCollections.observableArrayList();
        ArrayList<HighScore> data = HighScore.getHighScoresFromFile();
        data.sort(new HighScore(null,null,null,null));
        scoreData.addAll(data);
        high_table.setItems(scoreData);
    }

    /**
     * This method swaps current game scene to home page
     */
    @FXML
    public void backHome() {
        Main.toHomePage();
    }
}
