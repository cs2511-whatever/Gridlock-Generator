package whatever.gridlock;


/**
 * Enumerator Difficulty - representation of difficulty level of game
 * @author      Zhenyu Yao
 */
public enum Difficulty {
    EASY {
        @Override
        public String toString() {
            return "rookie";
        }
    },
    NORMAL {
        @Override
        public String toString() {
            return "normal";
        }
    },
    HARD {
        @Override
        public String toString() {
            return "veteran";
        }
    }
}
