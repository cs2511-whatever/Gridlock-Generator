package whatever.gridlock;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.application.Platform;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javazoom.jl.decoder.JavaLayerException;
import whatever.gridlock.model.GameSystem;
import whatever.gridlock.model.AudioPlayer;


/**
 * Class Main - main class for the application
 * @author      Zhenyu Yao
 * @author      Ruliang Wang
 * @author      Zhenqi Wang
 */
public class Main extends Application {

    private static Main instance;
    private Stage stage;
    private GameSystem system;

    /**
     * Main method
     * @param args      Command line arguments
     */
    public static void main(String[] args) {
        Main.launch(args);
	}

    @Override
    public void start(Stage primaryStage) {
        Main.instance = this;
        stage = primaryStage;
        stage.setResizable(false);
        stage.setTitle("Grid Lock Puzzle");
        switchFxmlPage("view/fxml/menu_ui.fxml");
        playMusic();
        stage.show();
    }

    /**
     * Method that plays the background music
     */
    private void playMusic() {
        new Thread(() -> {
            try {
                new AudioPlayer(new File("data/song.mp3")).play();
            } catch (FileNotFoundException | JavaLayerException e) {
                e.printStackTrace();
            }
        }).start();
    }

    /**
     * Method that starts the game and load the ui
     */
    public static void startGame(whatever.gridlock.Difficulty difficulty) {
        Main.instance.system = new GameSystem(difficulty);
        Main.instance.switchFxmlPage("view/fxml/game_ui.fxml");
        Main.instance.stage.show();
    }

    /**
     * Method that switches current page to score board
     */
    public static void toScoreBoard() {
        Main.instance.switchFxmlPage("view/fxml/score_board_ui.fxml");
        Main.instance.stage.show();
    }

    /**
     * Method that switches current page to help
     */
    public static void toHelpPage() {
        Main.instance.switchFxmlPage("view/fxml/help_page_ui.fxml");
        Main.instance.stage.show();
    }

    /**
     * Method that goes back to home page
     */
    public static void toHomePage() {
        Main.instance.switchFxmlPage("view/fxml/menu_ui.fxml");
        Main.instance.stage.show();
    }

    /**
     * Method that goes to difficulty selection page
     */
    public static void toDifficultyPage(){
        Main.instance.switchFxmlPage("view/fxml/level_selection_ui.fxml");
        Main.instance.stage.show();
    }

    /**
     * Method that exits the program
     */
    public static void exitProgram() {
        try {
            Platform.exit();
            System.exit(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method that switches between the FXML files
     * @param fxmlFile  The url for the fxml file
     */
    private void switchFxmlPage(String fxmlFile) {
        Parent newParent;
        try {
            newParent = FXMLLoader.load(this.getClass().getResource(fxmlFile));
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("no such fxml file");
        }
        if (this.stage.getScene() == null) {
            double width = 360;
            double height = 480;
            Scene scene = new Scene(newParent, width, height);
            this.stage.setScene(scene);
        }
        else {
            this.stage.getScene().setRoot(newParent);
        }
    }

    /**
     * Method that gets the instance of game system
     * @return          The game system
     */
    public static GameSystem getGameSystem() {
        return Main.instance.system;
    }
}
